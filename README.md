General structure of each file:

database server:

1. Request a local TCP port

	a. Create TCP socket
	b. Bind the socket 
	c. Set it to listening.
	
2. Broadcasts a registration message in UDP
	a. Create a UDP socket
	b. Bind the socket
	c. Create the message containing IP and port
	d. Send the message using the UDP socket
	e. Receive a message from the service-map server saying "OK".  
	
3. Perform commands for client

	a. Accept connection from client
	b. Fork a child process that keeps reading 
	c. Read command and query or update db17 
	d. (Close new socket once "quit" is received) Client won't send "quit" to server  

service-map server:

1. Accept registration from db server and communicate with client
	a. Create a UDP socket (this is the server side of UDP socket)
	b. Bind the socket
	c. Read a message
		i. If it starts with "PUT BANK620", store IP and port and send back "OK"
		ii. If it is "GET BANK620", send IP and port (comma-separated string)
	d. Questions:
		i. When should we close the TCP port? I think when it is done sending the IP and port to the client.
		ans. 
		ii. Do we need just one UDP socket or two?

client:

1. Broadcast message requesting db server info
	a. Create a UDP socket
	b. Bind the socket
	c. Send the message "GET BANK620" to the socket
	d. Receive IP and port of db server stored in comma-separated string
2. Communicate with db server
	a. Create TCP socket
	
	b. Prompt user for commands using while loop.
	
		i. Connect to the db server using TCP socket
		ii. Encode the commands into a structure
		iii. Send the commands to server in binary data (Encode BigEndian)
		iv. Get database queried/updated data and close connection
	    v. If the user types "quit", stop looping and exit program
		
	

Test cases:

1. Mutex on updateDB

	a. simplify server.c to be a TCP server
	
	b. write two to three TCP clients tcp_client1.c, tcp_client2.c, tcp_client3.c and all of them try to update the same account at the same time
	
	c. print access sequences for debug
		
Debug Experiences:

In the client.go file, I used the net.DialUDP method for UDP broadcasting. One of the parameters for this method is a pointer to a net.UDPAddr structure. 
However, at first, I was trying to simply give it a string such as “137.148.205.255:30854”. I had to change this to a net.UDPAddr structure and pass its 
address to the net.DialUDP method. After I did this, the go program successfully compiled. 

In the server.c file, I kept getting an error when broadcasting the “PUT BANK620” message. After printing out the error message, I saw that it said that 
I was trying to perform a socket action on an invalid socket. From here, I realized something must have been incorrect with the socket creation line of code, 
even though that line did not return -1 and give me an error. The reason for this is because my initial line of code was the following: 
if(udp_sk = socket(AF_INET,SOCK_DGRAM,0) < 0). As you can see, I did not wrap udp_sk = socket(AF_INET, SOCK_DGRAM,0) in parentheses. 
Because of this mistake, the socket was not correctly being created and assigned to udp_sk, so when I was trying to broadcast a message, 
it was not correctly working. To fix this, I changed that if statement to: if((udp_sk = socket(AF_INET,SOCK_DGRAM,0)) < 0)
and this allowed the broadcasting to work. 

When running the client executable, I cam across an issue where the client was unable to connect to the database server. The error message was: 
"tcp dial connection refused". After some research, I found that the specified port the client was trying to connect to was not open for listening. 
This is because I sent the port numberfrom the database server to the servicemap in host byte order, whereas it should have been in network-byte order. 
After changing it to network-byte order, the client was able to connect to the database server.

While not a major issue, we noticed that when we edited and saved a new .go file, if it had already been compiled once, typing "make" would
not recompile the .go file, even if edits were made. To fix this issue and make the editing process easier, the first step
after "make" is typed is to remove the go executables so that it would recompile the .go files with the new edits.


##################################################################################
**********************************************************************************
TO-DO LIST

Suhua: 

Fixed a bug in updateDB, I am gonna to test mutex on update tomorrow (10/31/2017 updated)

Wrote a client_tcp.go which talks to server_tcp.c for query and update purpose. The data tranformation is still not working, try to fixed it tomorrow (11/3/2017)

trying to get correct query / update results back to client. (11/06/2017) 

Now tcp client and server can transfer data correcly. Will work on refactoring and merge it back to client.go and server.c (11/10/2017)
Suhua: You are right. The handout did mention it forks a child after it recieves a lookahead string. I will see if I can fix it. (11/15/2017)
Fixed fork(). I think it is ready to be submitted. Done with docment as well. Thank you very much for the hardwork! (11/15/2017)




Kaylee: Current getting held up with the clientUDP.go file. There's an issue with the null terminator in the byte array. Will try to fix tomorrow. (11/3/2017)
Fixed issue with null terminator. Trying to now fix issue with "tcp dial connection refused" when connecting client and db server (11/7/2017)
Fixed "connection refused" error. Added "make clean" to "make" step in order to rebuild go files. Now working on converting data to network byte order (11/9/2017)
Still not transferring data correctly. Need to figure out how to get data transferred in correct format from client to db server (11/10/2017)

Only thing left is to test and address the issue in the server.c file I added: the handout says that the parent forks a child once it
receives a message from the client. We are forking before this happens. So, I think we should move our first lookahead read
on the server-side up to the parent and wait for the first command to come in and then fork. What do you think? (11/11/2017)

