// DB Server
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <wait.h>
#include <fcntl.h>

#define BUF_SIZE 1024
#define SERV_MAP_PORT 30854
#define DB_FILE_NAME "db17"

char msg[BUF_SIZE];
static char buf[BUF_SIZE];

void signal_catcher(int sig) {
	wait(0);
}

/* Structure to use for database queries and updates */
struct record {
	int acctnum;
	char name[20];
	float value;
	char phone[12];
	int age;
};

/* Method to query the db to get back information on a particular person
   we do not need to lock the record because there are no edits */
struct record queryDB(int acct) {
	int fd = open(DB_FILE_NAME, O_RDONLY);
	//lseek(fd, 0, SEEK_SET);
	//get the size of the file
	struct stat statBuf;
	stat(DB_FILE_NAME, &statBuf);
	off_t size = statBuf.st_size;
	//loop through all of the records, inspecting the account number until it matches acct
	int i;
	struct record current;
	for(i = 0; i <= size - sizeof(struct record); i = i + sizeof(struct record)) {
		read(fd, &current, sizeof(struct record));
		if(current.acctnum == acct) {
			return current;
		}
	}
	printf("No such record !\n");
	exit(0);
}

/* Method to update the value of the account specified by the acctnum
   we DO need to lock the record because we will be editing it */
struct record updateDB(int acct, float amt) {
	int fd = open(DB_FILE_NAME, O_RDWR);
	//lseek(fd, 0, SEEK_SET);
	//get the size of the file
	struct stat statBuf;
	stat(DB_FILE_NAME, &statBuf);
	off_t size = statBuf.st_size;
	//loop through all of the records, inspecting the account number until it matches acct
	int i;
	struct record current;
	for(i = 0; i <= size - sizeof(struct record); i = i + sizeof(struct record)) {
		read(fd, &current, sizeof(struct record));
		if(current.acctnum == acct) {
			//lseek back to lock this record
			//after read(), it is pointing to the beginning of next record, so going one record back
			lseek(fd, -(sizeof(struct record)), 1); //now pointing at the beginning of the struct
			if(lockf(fd, F_LOCK, sizeof(struct record)) < 0) {
				printf("Couldn't lock record\n");
				//decide how to handle this error here
			}
			
			//read again in case we edited
			read(fd, &current, sizeof(struct record));
			lseek(fd, -(sizeof(struct record)), 1); //reset file pointer to front

			//now the record is locked, determine the new value and update it
			float newValue = amt + current.value;
			current.value = newValue;
			lseek(fd, (sizeof(int) + 20*sizeof(char)), 1); //now pointing at the value float
			write(fd, &newValue, sizeof(float));
			//go back to the beginning of this record and unlock it
			lseek(fd, -(sizeof(float) + 20*sizeof(char) + sizeof(int)), 1);
			lockf(fd, F_ULOCK, sizeof(struct record));
			return current;
		}
	}
	printf("No such record !\n");
	exit(0);
}

/* Method to get the db server's IP.  */
void getMyIpAddr(char *addrLocal) {	
	struct in_addr **addrList;
	//get host name (i.e. "monet")
	gethostname(addrLocal, BUF_SIZE);
	//get host IP address
	struct hostent *host = gethostbyname(addrLocal);
	char *ipAddr = (char *)malloc(BUF_SIZE*sizeof(char));
	addrList = (struct in_addr **) host->h_addr_list;
	ipAddr = inet_ntoa(*addrList[0]);
	memcpy(addrLocal, ipAddr, BUF_SIZE);
}

/* Method to convert the IP address and port into a string that will be broadcasted. */
void getMessageInBuf(char *ip_str, unsigned short port) {
	//get ip and port int arrays
	char *ip_values[4];
	int i = 0;
	char *token;
	while ((token = strsep(&ip_str, "."))) {
		ip_values[i] = token;
		i++;
	}
	int port_values[2];
	for(i = 1; i >= 0; i--) {
		port_values[i] = port % 256;
		port = port / 256;
	}
	
	//put the string in the buf char array
	sprintf(buf, "PUT BANK620 %s,%s,%s,%s,%d,%d", ip_values[0], ip_values[1], ip_values[2], ip_values[3], port_values[0], port_values[1]);
}

/* Method to create a UDP socket and broadcast message to service-map server. It will 
   also receive a confirmation. If the registration of the db server's IP and port is
   not confirmed, the program will exit. */
void udpBroadcast(int udp_sk, struct sockaddr_in remote_addr, unsigned short tcp_port) {
	/* Notes: the remote_addr refers to all the machines in the lab (using the network mask)
	   with a specific port number, based on our CSU ID. */
	
	//------------------------------ Set up socket for broadcasting ----------------------

	//set up UDP socket to broadcast registration message
    	if((udp_sk = socket(AF_INET,SOCK_DGRAM,0)) < 0) {
    		perror("Generate UDP socket error");
	    	exit(1);
    	}

    	memset(&remote_addr, 0, sizeof(remote_addr));
    	remote_addr.sin_family = AF_INET;
    	remote_addr.sin_addr.s_addr = inet_addr("137.148.205.255"); 
    	remote_addr.sin_port = ntohs(30854);
    
    	//need this to broadcast
    	setsockopt(udp_sk,SOL_SOCKET,SO_BROADCAST,&remote_addr,sizeof(remote_addr));
    
    	//-------------------- Send message to service-map server ---------------------------
    
    	//get the ip address of the db server (the machine this is executing on)
    	char *ip = (char *)malloc(BUF_SIZE*sizeof(char));
    	getMyIpAddr(ip);
    	
    	//get message to send in buffer
    	getMessageInBuf(ip,tcp_port);
    	if(sendto(udp_sk, buf, BUF_SIZE, 0, (struct sockaddr *) &remote_addr, sizeof(remote_addr)) < 0){
		perror("sendto error");
		close(udp_sk);
		exit(1);
	}
	
	memset(buf, 0, BUF_SIZE);
	socklen_t remote_addr_len = sizeof(remote_addr);
	if((recvfrom(udp_sk, buf, BUF_SIZE, 0, (struct sockaddr *)&remote_addr, &remote_addr_len)) < 0) {
		perror("recvfrom error");
		close(udp_sk);
		exit(1);
	} 
	    
    	//if the service-map server sent back an "OK" message
    	if(strcmp(buf, "OK") == 0) {
    		/* We can return back to the main method now and have the TCP socket wait until 
    	  	  it accepts a connection from the client, because the client can now get the 
    	   	  db server address from the service-map server */
    	   	printf("Registration OK from %s\n", inet_ntoa(remote_addr.sin_addr));
    	} 
   	else {
    		printf("Could not register IP and port with service-map server.\n");
    		exit(1);
    	}
    
    	//close the udp_sk
    	close(udp_sk);
}

int main() {
	/* The orig_tcp_sk will be the socket that listens for and accepts TCP connections.
	   The new_tcp_sk will be the socket that represents the connection between the db
	   server and the client. The udp_sk is used to broadcast a message to the 
	   service-map server and receive a confirmation.  */
	int orig_tcp_sk, new_tcp_sk, udp_sk;
	
	/* The db_server_addr refers to the socket address used for the server side of 
	   the TCP connection. The remote_addr is used to refer to the socket used in
	   the UDP socket to broadcast a message to all machines in the lab listening on a
	   specific port. It will send and receive data from to/from the service-map server.
	   The client_addr will be used once a connection is accepted using the TCP socket.
	   It represents the client that will communicate with the db server to query
	   the db17 database. */
	struct sockaddr_in db_serv_addr, remote_addr, client_addr;
	
	/* Lengths of different socket addresses */ 
	socklen_t client_addr_len, db_server_addr_len;
	
	//if error while cleaning up children 
	if(signal(SIGCHLD, signal_catcher) == SIG_ERR) {
		perror("SIGCHLD error");
		return 1;
	}
	
	//------------------- Create TCP socket and request port-----------------------------
		
	//create a socket using TCP protocol
	if((orig_tcp_sk = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("Generate TCP socket error");
		return 2;
	}
	
	//set up db server address
	memset(&db_serv_addr, 0, sizeof(db_serv_addr));
	db_serv_addr.sin_family = AF_INET;
	db_serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	db_serv_addr.sin_port = htons(0); //let the system assign a port

	db_server_addr_len = sizeof(db_serv_addr);
	//assign address for TCP socket
	if(bind(orig_tcp_sk, (struct sockaddr *) &db_serv_addr, sizeof(db_serv_addr)) < 0) {
		close(orig_tcp_sk);
		perror("Bind error");
		return 3;
	}

	//get the system-assigned port
	if(getsockname(orig_tcp_sk, (struct sockaddr *) &db_serv_addr, &db_server_addr_len) < 0) {
		close(orig_tcp_sk);
		perror("getsockname error");
		exit(1);
	}
	//-------------------------- Broadcast message in UDP -------------------------------
	
	udpBroadcast(udp_sk, remote_addr, htons(db_serv_addr.sin_port));
	
	/* When the code has returned here, we know that the db server IP and port has been
	   successfully registered in the service-map server. */
	   
	//---------------------- Listen for and accept connections --------------------------
	
	//listen for connections
	if(listen(orig_tcp_sk, 5) < 0) {
		close(orig_tcp_sk);
		perror("Listen error");
		return 4;
	}
	
	do {
		//create a new connection
    		client_addr_len = sizeof(client_addr);
    		if ((new_tcp_sk = accept(orig_tcp_sk, (struct sockaddr *) &client_addr,&client_addr_len)) < 0) {
      			close(orig_tcp_sk);
      			perror("accept error"); 
			return 5;
    		}

		//After connection is made and request is received, print out "service requested" message
		printf("Service Requested from %s\n", inet_ntoa(client_addr.sin_addr));


		/* For the connection, we will create a child process that will listen for commands 
           	   from the client and will execute them in the child process. */
        	pid_t cpid;
        	cpid = fork();
        	if(cpid < 0) {
                	printf("Error when forking.\n");
                	return 6;
        	}
		if (cpid == 0 ) { //child
			//create buffer
			int cmd, acctnum;
			float amt;
			//record to hold info to send back to client
			struct record rec;
			
			//get the command type
			if(read(new_tcp_sk, &cmd, 4) > 0) {
				if(ntohl(cmd) == 1001) {
					//query command!
					//get the acctnum
  					if (read(new_tcp_sk, &acctnum, 4) > 0 ){
  						acctnum = ntohl(acctnum);

  						rec = queryDB(acctnum);
  						char buffer[BUF_SIZE];
  						sprintf(buffer, "%s %d %.2f %s %d", rec.name, rec.acctnum, rec.value, rec.phone, rec.age);
  						
						//write string back to client
  						write(new_tcp_sk, &buffer, strlen(buffer)+1);
  					}
				}
				else if(ntohl(cmd) == 1002) {
					//update command!
					//get the acctnum
  					if (read(new_tcp_sk, &acctnum, 4) > 0 ){
  						acctnum = ntohl(acctnum);
  						
  						int amtbuf;
	  					//get the amt
  						if (read(new_tcp_sk, &amtbuf, 4) > 0 ){
  							amtbuf = ntohl(amtbuf);
  							memcpy(&amt, &amtbuf, 4);
	  						
							rec = updateDB(acctnum, amt);

	  						char buffer[BUF_SIZE];
	  						sprintf(buffer, "%s %d %.2f", rec.name, rec.acctnum,  rec.value);
	  						
	  						//write string back to client
							write(new_tcp_sk, &buffer, strlen(buffer)+1);
	  					}
					}
				}
			}
      			
      			close(new_tcp_sk);
      			return 0;
    		} else //parent
      		close(new_tcp_sk);
 	 } while( 1 );	//FOREVER

	//close the TCP connection
	close(orig_tcp_sk);

	return 0;
}