/* DB Server */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <wait.h>
#include <fcntl.h>

#define BUF_SIZE 1024
#define PORT 38012
#define DB_FILE_NAME "db17"

char msg[BUF_SIZE];
// static int buf[BUF_SIZE];

void signal_catcher(int sig) {
	wait(0);
}

/* Structure to use for database queries and updates */
struct record {
	int acctnum;
	char name[20];
	float value;
	char phone[12];
	int age;
};

/* Structure to determine what action the client is requesting
   if the client requests to query the db, "amount" will be not be set */
struct clientCommands {
	int cmd;
	int acctnum;
	int amount;
};

/* Method to query the db to get back information on a particular person
   we do not need to lock the record because there are no edits */
struct record queryDB(int acct) {
	int fd = open(DB_FILE_NAME, O_RDONLY);
	//get the size of the file
	struct stat statBuf;
	stat(DB_FILE_NAME, &statBuf);
	off_t size = statBuf.st_size;
	//loop through all of the records, inspecting the account number until it matches acct
	int i;
	struct record current;
	for(i = 0; i <= size - sizeof(struct record); i = i + sizeof(struct record)) {
		read(fd, &current, sizeof(struct record));
		if(current.acctnum == acct) {
			printf("Return a query record ! \n");
			return current;
		}
	}
	printf("No such record !\n");
	exit(0);
	//send back an error message saying that a record with that acctnum cannot be found
}

/* Method to update the value of the account specified by the acctnum
   we DO need to lock the record because we will be editing it */
void updateDB(int acct, int amt) {
	int fd = open(DB_FILE_NAME, O_RDWR);
	//get the size of the file
	struct stat statBuf;
	stat(DB_FILE_NAME, &statBuf);
	off_t size = statBuf.st_size;
	//loop through all of the records, inspecting the account number until it matches acct
	int i;
	struct record current;
	for(i = 0; i <= size - sizeof(struct record); i = i + sizeof(struct record)) {
		read(fd, &current, sizeof(struct record));
		if(current.acctnum == acct) {
			//lseek back to lock this record
			//after read(), it is pointing to the beginning of next record, so going one record back
			lseek(fd, -(sizeof(struct record)), 1); //now pointing at the beginning of the struct
			if(lockf(fd, F_LOCK, sizeof(struct record)) < 0) {
				printf("Couldn't lock record\n");
				//decide how to handle this error here
			}
			//now the record is locked, determine the new value and update it
			float newValue = amt + current.value;
			lseek(fd, (sizeof(int) + 20*sizeof(char)), 1); //now pointing at the value float
			write(fd, &newValue, sizeof(float));
			//go back to the beginning of this record and unlock it
			lseek(fd, -(sizeof(float) + 20*sizeof(char) + sizeof(int)), 1);
			lockf(fd, F_ULOCK, sizeof(struct record));
			return;
		}
	}
	//send back an error message saying that a record with that acctnum cannot be found
}

int main() {	   
	//---------------------- Listen for and accept connections --------------------------
	int orig_tcp_sk, new_tcp_sk;
	socklen_t client_addr_len;

	struct sockaddr_in client_addr, serv_adr;

	if (signal(SIGCHLD , signal_catcher) == SIG_ERR) {
    	perror("SIGCHLD"); return 1;
  	}

    //create socket
  	if((orig_tcp_sk = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
  		perror("socket error");
  		return 2;
  	}

  	serv_adr.sin_family      = AF_INET;            // Set address type
  	serv_adr.sin_addr.s_addr = INADDR_ANY;  // Any interface
  	serv_adr.sin_port        = ntohs(PORT);        
    
    // BIND
 	if (bind(orig_tcp_sk, (struct sockaddr *) &serv_adr,
        sizeof(serv_adr)) < 0){
	    close(orig_tcp_sk);
	    perror("bind error"); 
	    return 3;
  	}

  	printf("Binding succeed! \n");

	//listen for connections
	if(listen(orig_tcp_sk, 5) < 0) {
		close(orig_tcp_sk);
		perror("Listen error");
		return 4;
	}
	printf("Listen succeed! \n");
	
	do {
		//create a new connection
		client_addr_len = sizeof(client_addr);
		if ((new_tcp_sk = accept(orig_tcp_sk, (struct sockaddr *) &client_addr, &client_addr_len)) < 0) {
  			close(orig_tcp_sk);
  			perror("accept error"); 
		return 5;
		}
		printf("Connection succeed! \n");

		/* For the connection, we will create a child process that will listen for commands 
           	   from the client and will execute them in the child process. */
    	pid_t cpid;
    	cpid = fork();
    	if(cpid < 0) {
        	printf("Error when forking.\n");
        	return 6;
        }

		if (cpid == 0 ) { //child   
		    //create a local buffer
		    int cmd, acctnum; 
		    float amt;  
		    struct record queryrec; 


  			if (read(new_tcp_sk, &cmd, 4) > 0 ){
    		
  				if (ntohl(cmd)==1001) {// query command
  					printf("***get cmd query !\n");
  					if (read(new_tcp_sk, &acctnum, 4) > 0 ){
  						acctnum = ntohl(acctnum);
  						printf("get acctnum %d\n", acctnum);
  						
  						queryrec = queryDB(acctnum);

  						char buffer[BUF_SIZE];
  						sprintf(buffer, "acctnum: %d , Name: %s, Value: %f, Phone: %s, Age: %d\0", queryrec.acctnum, queryrec.name, queryrec.value, queryrec.phone, queryrec.age);
  						puts(buffer);

  						write(new_tcp_sk, &buffer, strlen(buffer)+1);
  					}

  				}
  				else if (ntohl(cmd)==1002) {//update command
  					printf("***get cmd update !\n");
  					
  					if (read(new_tcp_sk, &acctnum, 4) > 0 ){

  						acctnum = ntohl(acctnum);
  						printf("get acctnum %d\n", acctnum);

  						int amtbuf;
	  				

  						if (read(new_tcp_sk, &amtbuf, 4) > 0 ){
  							amtbuf = ntohl(amtbuf);
  							memcpy(&amt, &amtbuf, 4);
	  						printf("get amount %f\n", amt);
	  						updateDB(acctnum, amt);
	  						sleep(1);
                            

                            //send new record to client
	  						queryrec = queryDB(acctnum);
	  						char buffer[BUF_SIZE];
	  						sprintf(buffer, "acctnum: %d , Name: %s, New Value: %f, Phone: %s, Age: %d\0", queryrec.acctnum, queryrec.name, queryrec.value, queryrec.phone, queryrec.age);
	  						puts(buffer);
	  						write(new_tcp_sk, &buffer, strlen(buffer)+1);


	  					}
  					}
  				}
  				else {
  					printf("Wrong cmd\n");
  					exit(0);
  				}


  				

           }
      		close(new_tcp_sk);
      		return 0;
    	    
        }
		else //parent
      		close(new_tcp_sk);

 	} while( 1 );	//FOREVER

	//close the TCP connection
	close(orig_tcp_sk);

	return 0;
}