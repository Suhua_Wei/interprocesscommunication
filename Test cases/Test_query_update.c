#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#define DB_FILE_NAME "db17"


//function declaration , alter the queryDB return type for testing. 
struct record queryDB(int acct);

void updateDB(int acct, int amt);
/* Structure to use for database queries and updates */
struct record {
	int acctnum;
	char name[20];
	float value;
	char phone[12];
	int age;
};

void printRecord(struct record current){
	printf("\n\nPrint record:\n");
	printf("The acctnum %d :\n", current.acctnum);
    printf("The name %s :\n", current.name);
    printf("The value %f :\n", current.value);
    printf("The phone number %s :\n", current.phone);
    printf("The age %d :\n", current.age);
}

int main(int argc, char *args[]){
	//open data base 
	if(argc < 2){
		printf("Please provide acctnum: 11111\n");
		exit(1);
	}
    int acctnum;
	acctnum = atoi(args[1]);
	struct record query_record;
	// query_record = queryDB(acctnum);
	printRecord(queryDB(11111));	
	sleep(1);

	updateDB(acctnum, 1000.0);

	sleep(1);

	// query_record = queryDB(acctnum);
	printRecord(queryDB(11111));
	printRecord(queryDB(22222));

}