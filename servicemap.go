package main
 
import (
    "fmt"
    "net"
    "os"
    "strings"
)
 
/* A Simple function to verify error */
func CheckError(err error) {
    if err  != nil {
        fmt.Println("Error: " , err)
        os.Exit(0)
    }
}

func main() {
    /* create dictionary of names (this one will only contain the BANK620 name) */
    var names map[string]string
    names = make(map[string]string)
    
    /* Lets prepare an address at any address at port 30854*/   
    ServerAddr,err := net.ResolveUDPAddr("udp",":30854")
    CheckError(err)
 
    /* Now listen (misleading in Go;  actually bind at selected port */
    ServerCLNS, err := net.ListenUDP("udp", ServerAddr)
    CheckError(err)
    
    defer ServerCLNS.Close()

    for {
	//clear out the buffer every time
	buf := make([]byte, 1024)

    	/* after this, the message will be in buf */
        n,addr,err := ServerCLNS.ReadFromUDP(buf[0:])
	ip := addr.IP
        fmt.Println("Received from ", ip, ": ", string(buf[0:n]))
	
	//separate the message into each word
	command := strings.Fields(string(buf[0:]))
	
	//determine which command to do	
        if len(command) == 3 && command[0] == "PUT" {
		servName := command[1]
		ipPortStr := command[2]
		names[servName] = ipPortStr
      		
		//return an "OK" registration 
        	_,err := ServerCLNS.WriteToUDP([]byte("OK"), addr)
        	CheckError(err)
        } else if len(command) == 2 && command[0] == "GET" {
		servName := strings.TrimRight(string(command[1][0:]), "\x00")
		value := names[servName]
		_, err := ServerCLNS.WriteToUDP([]byte(value), addr)
		CheckError(err) 
        } else {
		_,err := ServerCLNS.WriteToUDP([]byte("Invalid request"), addr)
		CheckError(err)
	}
 
        if err != nil {
            fmt.Println("Error: ",err)
        } 
    }
}
