all: cleanPreCompile server servicemap client

server: server.o
	gcc -I.  -o server server.o

servicemap:
	go build servicemap.go

client:
	go build client.go

cleanPreCompile:
	rm -f servicemap client

clean: 
	rm -f server.o servicemap client server

depend: 
	makedepend -I. server.c
# DO NOT DELETE THIS LINE - make depend depends on it.

server.o: /usr/include/sys/types.h /usr/include/sys/socket.h /usr/include/sys/un.h /usr/include/sys/stat.h 
server.o: /usr/include/netdb.h /usr/include/arpa/inet.h /usr/include/unistd.h /usr/include/stdlib.h
server.o: /usr/include/stdio.h /usr/include/signal.h /usr/include/wait.h /usr/include/fcntl.h
