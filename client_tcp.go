package main 

import (
	"net"
	"fmt"
	"os"
	// "io"
	"bufio"
	"encoding/binary"
	"strconv"
	"strings"
	"math"
)
/* A Simple function to verify error */
func CheckError(err error) {
    if err  != nil {
        fmt.Println("Error: " , err)
        os.Exit(0)
    }
}
/* A function converting float to bytes and BigEndian */
func FloatToBytes(flo float32) []byte {
	bits := math.Float32bits(flo)
	bytes := make([]byte, 4)
	binary.BigEndian.PutUint32(bytes[0:], bits) //the BigEndian function is similar to ntohl()
	return bytes
}
func  IntToBytes(num uint32) []byte{
	bytes := make([]byte, 4)
	binary.BigEndian.PutUint32(bytes[0:], num)
	return bytes
}

func main() {
   if(len(os.Args) < 3 ) {
      fmt.Println("need server and port!")
      os.Exit(1)
   }
    for{
   	
	   	fmt.Print(">")
	   	reader := bufio.NewReader(os.Stdin)
	    text, _ := reader.ReadString('\n')
	    
	    //split the command

	    cmds := strings.Fields(text)

	   	var lookhead string
	
        
        // fmt.Scanf("%s ", &lookhead);
        lookhead = cmds[0]

        //create TCP connection 
	    serverip := os.Args[1]
	    port := os.Args[2]
	    conn, err := net.Dial("tcp", serverip +":"+ port)
	    CheckError(err)


	    if lookhead == "query" {
	    	cmd := uint32(1001)	
	    	acctnum, err := strconv.Atoi(cmds[1])
	    	CheckError(err)

	    	bytecmd := IntToBytes(cmd)
        	byteacc := IntToBytes(uint32(acctnum))
        	
        	//send cmds
        	conn.Write(bytecmd)
        	conn.Write(byteacc)
    
	    } else if lookhead == "update" {
	    	acctnum, err := strconv.Atoi(cmds[1])
	    	CheckError(err)
	    	amt, err := strconv.ParseFloat(cmds[2],32)
	    	CheckError(err) 	

	    	// convert to byte and BigEndian  
	    	bytecmd := IntToBytes(uint32(1002))
            byteacc := IntToBytes(uint32(acctnum))
            byteamt := FloatToBytes(float32(amt))

        	//send cmds
        	conn.Write(bytecmd)
        	conn.Write(byteacc)
        	conn.Write(byteamt)
  
	    } else {
	    	fmt.Println("Wrong command, continue\n")
	    	conn.Close()
	    	continue
        }

        //get record from server
    	buffer := make([]byte, 1024)
	    conn.Read(buffer)
	    fmt.Printf("%s\n",buffer)
		
        //close the connection after sending cmds
        conn.Close()
	    
    }
}

