package main
 
import (
    "os"
    "fmt"
    "net"
    "strings"
    "strconv"
    "encoding/binary"
    "math"
    "bufio"
)


/* A function converting float to bytes and BigEndian */
func FloatToBytes(flo float32) []byte {
	bits := math.Float32bits(flo)
	bytes := make([]byte, 4)
	binary.BigEndian.PutUint32(bytes, bits) //the BigEndian function is similar to ntohl()
	return bytes
}

func  IntToBytes(num uint32) []byte{
	bytes := make([]byte, 4)
	binary.BigEndian.PutUint32(bytes, num)
	return bytes
}

func CheckError(err error) {
	if err  != nil {
        	fmt.Println("Error: " , err)
		os.Exit(1)
    	}
}

//method that returns string in format "ip:port" for db server
func getAddrStr(addrStr string) string {
	ipAndPortArray := strings.Split(addrStr, ",")
	ip_str := ipAndPortArray[0] + "." + ipAndPortArray[1] + "." + ipAndPortArray[2] + "." + ipAndPortArray[3] 
	mostSigPort, err := strconv.Atoi(ipAndPortArray[4])
	CheckError(err)
	leastSigPort, err := strconv.Atoi(string(ipAndPortArray[5]))
	CheckError(err)
	portInt := 256*mostSigPort + leastSigPort
	
	dbServerAddrStr := ip_str + ":" + strconv.Itoa(portInt)
	
	return dbServerAddrStr
}

//broadcasts the message "GET BANK620" to service map which returns 
//comma-delimited string containing ip and port 
func getDbServerAddr() string {
    //broadcast message to all workstations in lab at port 30854
    broadcastAddr := net.UDPAddr{IP: net.ParseIP("137.148.205.255"), Port: 30854}
    CLNS, err := net.DialUDP("udp",nil, &broadcastAddr)
    CheckError(err)

    defer CLNS.Close()
    
    //send message "GET BANK620" 
    buf := []byte("GET BANK620")
    _,err = CLNS.Write(buf)
    CheckError(err)
    
    //listen for the response message
    localAddr,errLocal := net.ResolveUDPAddr("udp", CLNS.LocalAddr().String())
    CheckError(errLocal)

    err = CLNS.Close()
    CheckError(err)

    CLNS,err = net.ListenUDP("udp", localAddr)
    CheckError(err)

    //receive ip address and port in comma-delimited string from service map server
    response := make([]byte, 100)
    _,_,err = CLNS.ReadFromUDP(response[0:])
    CheckError(err)
    
    //get the new string in form: "xxx.xxx.xxx.xxx:port"
    responseStr := strings.TrimRight(string(response), "\x00")
    dbServAddrStr := getAddrStr(responseStr)
    
    //close the CLNS 
    CLNS.Close()
   
    return dbServAddrStr
}
 
func main() {
    // ---------------------- GET BANK620 ADDRESS ---------------------------------------
    dbServerAddr := getDbServerAddr()
     	
    // ----------------------- GET TCP SOCKET -------------------------------------------
    
    //write out who the service is provided by here
    serviceProvider := strings.Split(dbServerAddr, ":")
    fmt.Println("Service provided by ", serviceProvider[0], " at port ", serviceProvider[1])
	

    //create infinite loop that stops when user types in "quit"
    for {
	//form TCP connection
    	conn, err := net.Dial("tcp", dbServerAddr)
    	CheckError(err)
    	defer conn.Close()

	//prompt user for input
    	fmt.Print("> ")
    	
	//prompt user for commands
    	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	    
	//split the command
	cmds := strings.Fields(text)

	var lookahead string
        lookahead = cmds[0]

	if lookahead == "query" && len(cmds) == 2 {
		cmd := uint32(1001)	
	    	acctnum, err := strconv.Atoi(cmds[1])
	    	CheckError(err)

	    	bytecmd := IntToBytes(cmd)
        	byteacc := IntToBytes(uint32(acctnum))
        	
        	//send cmds
        	conn.Write(bytecmd)
        	conn.Write(byteacc)

	}else if lookahead == "update" && len(cmds) == 3 {
		acctnum, err := strconv.Atoi(cmds[1])
	    	CheckError(err)
	    	amt, err := strconv.ParseFloat(cmds[2],32)
	    	CheckError(err) 	

	    	// convert to byte and BigEndian  
	    	bytecmd := IntToBytes(uint32(1002))
                byteacc := IntToBytes(uint32(acctnum))
                byteamt := FloatToBytes(float32(amt))

        	//send cmds
        	conn.Write(bytecmd)
        	conn.Write(byteacc)
        	conn.Write(byteamt)

	}else if lookahead == "quit" && len(cmds) == 1 {
		conn.Close()
		os.Exit(1)
	}else {
		fmt.Println("Invalid command. Try again.")
		conn.Close()
		continue
	}
	
	//get record from server
	buffer := make([]byte, 1024)
	conn.Read(buffer)
	fmt.Printf("%s\n",buffer)
	
	//close the connection after sending cmds
	conn.Close()
    }	
}
