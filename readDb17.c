#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <wait.h>
#include <fcntl.h>

#define DB_FILE_NAME "db17"

struct record {
	int acctnum;
	char name[20];
	float value;
	char phone[12];
	int age;
};

int main() {
	int fd = open(DB_FILE_NAME, O_RDONLY);
	//get the size of the file
	struct stat statBuf;
	stat(DB_FILE_NAME, &statBuf);
	off_t size = statBuf.st_size;
	//loop through all of the records, inspecting the account number until it matches acct
	int i;
	struct record current;
	for(i = 0; i <= size - sizeof(struct record); i = i + sizeof(struct record)) {
		read(fd, &current, sizeof(struct record));
		printf("acctnum: %d, name: %s, value: %.1f, phone: %s, age: %d\n", current.acctnum, current.name, current.value, current.phone, current.age);
	}

	return 0;
}
